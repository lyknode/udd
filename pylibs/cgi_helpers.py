# -*- coding: utf-8; mode: python; tab-width: 4; -*-
# Python3 script
# () 2018, by nyov
# Public Domain

import sys
import os
import codecs
import cgitb
from cgitb import Hook


CGI_ENCODING = 'utf-8'

# Enforce UTF-8 output,
# detach binary buffer from _io.TextIOBase
if sys.version_info < (3, 1):
    sys.stdout = codecs.getwriter(CGI_ENCODING)(sys.stdout)
else:
    sys.stdout = codecs.getwriter(CGI_ENCODING)(sys.stdout.detach())

    # Fix the cgitb handler trying to write to the detached buffer
    def enable(*args, file=sys.stdout, **kwargs):
        sys.excepthook = Hook(*args, file=file, **kwargs)
    cgitb.enable = enable

# cgitb.enable(format='text')  # html format is writing output before header, bad
cgitb.enable()

# As per HTTP1.1 spec, HTTP Header line-endings are CRLF
# (RFC 2616 Sec. 4.1; RFC 7230 Sec. 3)
CR = '\r'
LF = '\n'
CRLF = CR + LF


# Tiny helper to write correct CRLF linebreaks
def print_contenttype_header(contenttype, encoding=CGI_ENCODING):
    return sys.stdout.write('Content-Type: {0}; charset={1}{2}'.
                            format(contenttype, encoding, CRLF+CRLF))


# Script location on the net
# for self-reference
try:
    PAGE_URL = os.environ['REQUEST_SCHEME'] + '://' + os.environ['HTTP_HOST'] + os.environ['SCRIPT_NAME']
except KeyError:
    PAGE_URL = 'N/A ' + __file__
