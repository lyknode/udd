The upload-history importer is a bit complex.

As input data, it uses mbox email archives. Those are rsynced manually every
few years from master.debian.org, to
/srv/udd.debian.org/email-archives/debian-devel-changes/.  To complement those,
udd+changes@ is subscribed to debian-devel-changes, and messages are received
to /srv/udd.debian.org/incoming-mail/changes (symlinked to
/srv/udd.debian.org/email-archives/debian-devel-changes/debian-devel-changes.current
)

From time to time, to reduce the size taken by that file, it is a good idea to reset things, and:
- rsync archives from master.debian.org
- truncate /srv/udd.debian.org/incoming-mail/changes

Then the importer is a two step process.

First, it runs:
  cd /srv/udd.debian.org/upload-history && make -f /srv/udd.debian.org/udd/scripts/Makefile.upload-history
That generates files in /srv/udd.debian.org/upload-history/ with a summary of each upload.

Then, the importer itself runs and parses those .deb822-list files, and imports
them into the DB.
