#!/bin/sh

set -e

TARGETDIR="/srv/udd.debian.org/mirrors/lintian"
BASEURL="https://lintian.debian.org/results"

mkdir -p "$TARGETDIR"
rm -rf "$TARGETDIR/results"

cd "$TARGETDIR"
wget -q --ca-directory /etc/ssl/ca-debian "$BASEURL/all.tar" -O "$TARGETDIR/all.tar"
tar xf "$TARGETDIR/all.tar"
test -d "$TARGETDIR/results"
